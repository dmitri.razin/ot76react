import axios from 'axios';

import { apiPrefix } from '../config/config.json';

export default {
    getTransports(){
        return axios.post(`${apiPrefix}/parse.php`, {
                action: "getallroutes"
        });
    },
}
