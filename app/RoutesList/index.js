import React, { Component } from 'react';
// import api from './../api/request.js'
// import { YMaps, Map, GeoObject, Placemark } from 'react-yandex-maps';




// var Flux = require('fluxify');
// var dispatcher = require('fluxify').dispatcher;
//
// var MainScreen = Flux.createStore({
//     id: 'MainScreen',
//     initialState: {
//         trList: {}
//     },
//     actionCallbacks: {
//         setMarsh:  function ( updater, data ) {
//             console.log(`setMarsh`, data);
//
//             updater.set( { trList: data } );
//         },
//     }
// });
//
// var callbackId = dispatcher.register( function( payload ){
//     switch (payload.actionType){
//         case 'LOAD_MARH_SUC': {
//             console.log(arguments[0]);
//
//             Flux.doAction( 'setMarsh', arguments[0] );
//
//             break;
//         }
//     }
//
//
//
// });

// import placemarks from './placemark.json';
//
// const mapState = { center: [57.626569, 39.893787], zoom: 10 };



class RoutesList extends Component {

    constructor(props){
        super(props);


        this.state = {
            showAutobus: false,
            showTrallbus: false,
            showTramv: false,
            showMarsh: false,
        };
    }

    // componentWillMount(){
    //     api.getTransports().then((data)=>{
    //
    //         dispatcher.dispatch( {actionType: 'LOAD_MARH_SUC', trList: data.data} );
    //     });
    // }
    toggleShow(name){
        let tmp = this.state;
        tmp[name] = !tmp[name];
        this.setState(tmp);
    }

    render(){
        return(
            <div>

                <div className="search">
                    <input type="text" id="searchfield" placeholder="Поиск" disabled="true" />
                </div>

                <div className="items">
                    <div className="group group-autobus">
                        <div className="name" onClick={()=>{this.toggleShow("showAutobus")}}>Автобусы</div>
                        <div className={this.state.showAutobus?"list":"list hide"}>
                            {this.props.list.autobus.map((item)=>{
                                return <div className={item.active?"item active":"item"} onClick={()=>{
                                    this.props.changeStatus('autobus', item.text_id);
                                }}>
                                    <div className="num">{item.text_id}</div>
                                    <div className="route">
                                        <span className="f">{item.from}</span>
                                        <span className="l">{item.to}</span>
                                    </div>
                                </div>
                            })}
                        </div>
                    </div>

                    <div className="group group-trallbus">
                        <div className="name" onClick={()=>{this.toggleShow("showTrallbus")}}>Троллейбусы</div>
                        <div className={this.state.showTrallbus?"list":"list hide"}>
                            {this.props.list.trallbus.map((item)=>{
                                return <div className={item.active?"item active":"item"} onClick={()=>{
                                    this.props.changeStatus('trallbus', item.text_id);
                                }}>
                                    <div className="num">{item.text_id}</div>
                                    <div className="route">
                                        <span className="f">{item.from}</span>
                                        <span className="l">{item.to}</span>
                                    </div>
                                </div>
                            })}
                        </div>
                    </div>

                    <div className="group group-tramv">
                        <div className="name" onClick={()=>{this.toggleShow("showTramv")}}>Трамваи</div>
                        <div className={this.state.showTramv?"list":"list hide"}>
                            {this.props.list.tramv.map((item)=>{
                                return <div className={item.active?"item active":"item"} onClick={()=>{
                                    this.props.changeStatus('tramv', item.text_id);
                                }}>
                                    <div className="num">{item.text_id}</div>
                                    <div className="route">
                                        <span className="f">{item.from}</span>
                                        <span className="l">{item.to}</span>
                                    </div>
                                </div>
                            })}
                        </div>
                    </div>

                    <div className="group group-marsh">
                        <div className="name" onClick={()=>{this.toggleShow("showMarsh")}}>Маршрутки</div>
                        <div className={this.state.showMarsh?"list":"list hide"}>
                            {this.props.list.marsh.map((item)=>{
                                return<div className={item.active?"item active":"item"} onClick={()=>{
                                    this.props.changeStatus('marsh', item.text_id);
                                }}>
                                    <div className="num">{item.text_id}</div>
                                    <div className="route">
                                        <span className="f">{item.from}</span>
                                        <span className="l">{item.to}</span>
                                    </div>
                                </div>
                            })}
                        </div>
                    </div>

                </div>
            </div>
        );
    }

    // componentDidMount(){
    //
    //     var me = this;
    //     MainScreen.on( 'change:trList', function( value ){
    //         me.setState( { trList: value.trList } );
    //     });
    // }
}

export { RoutesList };