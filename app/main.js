// main.js
import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import { Home } from './Home/index.js'
import './main.less';

const render = function (){
    ReactDOM.render(<AppContainer><Home/></AppContainer>, document.getElementById('root'));
};

render();

// Webpack Hot Module Replacement API
if (module.hot) {
    module.hot.accept('./Home/index.js', () => { render() })
}