import React, { Component } from 'react';
import api from './../api/request.js'
import { YMaps, Map, GeoObject, Placemark } from 'react-yandex-maps';
import { RoutesList } from './../RoutesList'




var Flux = require('fluxify');
var dispatcher = require('fluxify').dispatcher;

var MainScreen = Flux.createStore({
    id: 'MainScreen',
    initialState: {
        trList: {
            autobus: [],
            marsh: [],
            trallbus: [],
            tramv: [],
        }
    },
    actionCallbacks: {
        setMarsh:  function ( updater, data ) {
            console.log(`setMarsh`, data);

            updater.set( { trList: data } );
        },
    }
});

var callbackId = dispatcher.register( function( payload ){
    switch (payload.actionType){
        case 'LOAD_MARH_SUC': {
            console.log(arguments[0]);

            Flux.doAction( 'setMarsh', arguments[0] );

            break;
        }
    }



});

// import placemarks from './placemark.json';

const mapState = { center: [57.626569, 39.893787], zoom: 14 };



class Home extends Component {

    constructor(props){
        super(props);


        this.state = {
            placemarks: [],
            trList: {
                autobus: [],
                marsh: [],
                trallbus: [],
                tramv: [],
            },
        };
    }

    componentWillMount(){
        api.getTransports().then((data)=>{

            let list = data.data;
            list.autobus = list.autobus.map((item)=>{
                item.active = false;
                return item;
            });

            dispatcher.dispatch( {actionType: 'LOAD_MARH_SUC', trList: list} );
        });
    }
    toggleListItem(type, id){
        let tmp = this.state;
        tmp.trList[type] = tmp.trList[type].map((item)=>{
            if(item.text_id == id){
                item.active = !item.active;
            }
            return item;
        });
        this.setState(tmp);
    }
    render(){
        return(
            <div className="container">

                <div className={"yandex"}>
                    <YMaps>
                        <Map state={mapState} width={"100%"} height={"100%"}>

                        </Map>
                    </YMaps>
                </div>

                <div className="overlay">
                    <RoutesList list={this.state.trList} changeStatus={(i,a)=>{this.toggleListItem(i,a);}}/>
                </div>

            </div>
        );
    }

    componentDidMount(){

        var me = this;
        MainScreen.on( 'change:trList', function( value ){
            me.setState( { trList: value.trList } );
        });

    }
}

export { Home };