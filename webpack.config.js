//
// module.exports = {
//     entry: "./app/main.js",
//     output: {
//         path: __dirname + "/dist",
//         //publicPath: 'app/',
//         filename: 'build.js'
//     },
//     //externals: [nodeExternals()],
//     module: {
//         loaders: [{
//             test: /\.js$/,
//             exclude: /node_modules/,
//             loaders: ['react-hot', 'babel-loader?presets[]=react,presets[]=es2017,presets[]=stage-0']
//         }],
//         rules: [{
//             test: /\.less$/,
//             use: [{
//                 loader: "style-loader" // creates style nodes from JS strings
//             }, {
//                 loader: "css-loader" // translates CSS into CommonJS
//             }, {
//                 loader: "less-loader" // compiles Less to CSS
//             }]
//         }]
//         // loaders: [
//         //     {
//         //         test: /\.(js|jsx)$/,
//         //         loader: 'babel-loader',
//         //
//         //         query: {
//         //             presets: ['es2017', 'react']
//         //         }
//         //     }
//         // ]
//     }
// };
//

require('webpack');

module.exports = {
    context: __dirname + '/app',
    entry: {
        javascript: "./main.js",
        //html: "./index.html"
    },
    output: {
        filename: 'build.js',
        path: __dirname + '/dist'
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loaders: ['react-hot-loader/webpack', 'babel-loader?presets[]=react,presets[]=es2015'],
                // loaders: ["react-hot", 'babel-loader'],
                // query: {
                //    presets : ['es2015', 'react']
                // }
            },
            { test: /\.less$/, loader: "style-loader!css-loader!less-loader" }
            // {
            //     test: /\.html$/,
            //     loader: "file?name=[name].[ext]"
            // }
        ],

    },
    devServer: {
        contentBase: __dirname + '/dist',
        compress: true,
        port: 9000,
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    }
};